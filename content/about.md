+++
title = "À propos"
+++

## Bonjour à tous

**Code Ouvert** est un blog de codage où je publie des artlcles liés à HTML, CSS, JavaScript, PHP... ainsi que des framework tels que symfony, laravel... Ici, je fournis également gratuitement les codes sources de chacune de mes vidéos YouTube et vous pouvez utiliser ces codes sans aucune restriction ni limitation. Je crois que mes vidéos ou codes aident à inspirer les concepteurs et développeurs Web et aident également à améliorer leurs compétences.

Je m'appelle **Yassin El Kamal NGUESSU** et je suis un **développeur backend** d'abord autodidacte puis diplomé d'une licence en genie informatique. J'ai également travaillé sur de nombreux **projets front-end** dans le passé et j'y travaille toujours. Internet et le développement Web sont ma passion et je crois qu'il est important d'aider les gens avec mes capacités et mes connaissances. J'apprends ces choses depuis 2016 et j'ai l'impression que l'apprentissage fait désormais partie de ma vie.

J'ai également une [**chaîne YouTube**](https://www.youtube.com/c/CodeOuvert) sur laquelle je télécharge des didacticiels de conception et de développement Web qui rendent cette expérience encore plus intéressante. J'espère que vous apprécierez mon contenu ici et je vous remercie d'avoir pris le temps de lire ceci.

Restez connecté avec nous :

- GitHub – https://github.com/code-ouvert
- GitLab – https://gitlab.com/code-ouvert
- YouTube – https://www.youtube.com/c/CodeOuvert

Si vous avez des questions, n'hésitez pas à nous contacter :

>Téléphone : +237 6 81 07 60 39
>
>Whatsapp :  +237 6 81 07 60 39
>
>Adresse e-mail: leyassino@gmail.com 
