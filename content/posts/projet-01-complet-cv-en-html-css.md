---
title: "Projet 01 (Complet) : CV en HTML / CSS"
date: 2022-06-18T07:00:00+01:00
author: "Yassin El Kamal NGUESSU"
authorTwitter: "" # do not include @
cover: "" # /img/projet01/01.png
tags: ["cv", "html", "css", "responsive", "design", "le developpeur web"]
keywords: ["code ouvert", "article", "creer un blog avec hugo", "le developpeur web"]
description: "Qu’est ce que nous allons voir aujourd’hui? Dans ce nouveau projet, je vous propose de créer une page complète en HTML et en CSS qui sera une page de CV. Pour cela, nous allons utiliser les notions de flexbox, le responsive design et les éléments HTML5 structurants."
showFullContent: false
draft: false
---
{{< youtube hZ0ja7d3eHE >}}

## Objectifs

Qu’est ce que nous allons voir aujourd’hui? Dans ce nouveau projet, je vous propose de **créer une page complète en HTML et en CSS** qui sera une **page de CV**. Pour cela, nous allons utiliser les **notions de flexbox, le responsive design et les éléments HTML5 structurants.**

## Pré-requis

- Les base en HTML5 et CSS3

## Outils de développement

- Un ordinateur (la précision en vaut la peine)

- Un Editeur de text (Exemple : [Sublime Text](https://www.sublimetext.com/), [visual Studio Code](https://code.visualstudio.com/), ...)

- Trois Navigateurs ou plus pour les tests (Google Chrome, Mozilla Firefox
, Microsoft Edge, ...)

## Ressources

- https://www.youtube.com/watch?v=hZ0ja7d3eHE

## Création des dossiers et fichiers du projet

```
cv/
├── assets/
│   ├── css/
│   │   ├── style.css
│   │
│   ├── files/
│   │	├── cv.pdf
│   │   
│   ├── images/
│	├── default_user.jpg
│
├── index.html
│   
├── LICENSE
```

## MIT License

```License
MIT License

Copyright (c) 2022 Le Développeur Web <leyassino@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
```

## Structure minimale d’une page HTML5 valide

Nous commençons par créer la structure HTML avant de définir les styles particuliers en CSS.

```html
<!DOCTYPE html>
<html lang="fr">

	<head>
		<meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>...</title>
	</head>

	<body>
		...
	</body>

</html>
```

## Code source

- [GitLab]()
