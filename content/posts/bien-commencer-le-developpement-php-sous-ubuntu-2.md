---
title: "Bien commencer le développement PHP sous Ubuntu Eps2 : installation de PHPMYADMIN"
date: 2021-10-23T10:00:00+01:00
author: "Yassin El Kamal NGUESSU"
authorTwitter: "" # do not include @
cover: "" # /img/projet01/01.png
tags: ["php", "ubuntu", "MySQL", "apache2", "phpmyadmin", "linux", "le developpeur web"]
keywords: ["code ouvert", "php", "ubuntu", "MySQL", "apache2", "lamp", "linux", "phpmyadmin", "Bien commencer le développement PHP sous Ubuntu", "le developpeur web"]
description: "C’est parti, vous voulez vous lancer dans Ubuntu pour faire du développement PHP, mais vous ne savez pas par où commencer? Qu’est ce que nous allons voir aujourd’hui ? Installation de PHPMYADMIN et création d’un utilisateur admin."
showFullContent: false
draft: false
---
{{< youtube 8DeF2qZezIY >}}

## Objectifs

C'est parti, vous voulez vous lancer dans **Ubuntu** pour faire du **développement PHP**, mais vous ne savez pas par où commencer?

Qu'est ce que nous allons voir aujourd'hui ? **Installation de PHPMYADMIN et création d'un utilisateur admin.**

## Pré-requis : 

- Une installation fraîche (ou à peu près fraîche) d'Ubuntu.

- Installation de PHP, MySQL, APACHE2 ([Voir Eps 1](https://code-ouvert.gitlab.io/code-ouvert/posts/bien-commencer-le-developpement-php-sous-ubuntu-1/))

## Ressources :

- https://doc.ubuntu-fr.org/lamp

