+++
title = "Démarrage"
weight = 5
pre = "<b>3. </b>"
chapter = true
+++

# 🚀 Code Ouvert 🚀

**Code Ouvert Web est un blog de codage où je publie des artlcles liés à HTML, CSS, JavaScript, PHP… ainsi que des framework tels que symfony, laravel… Ici, je fournis également gratuitement les codes sources de chacune de mes vidéos YouTube et vous pouvez utiliser ces codes sans aucune restriction ni limitation. Je crois que mes vidéos ou codes aident à inspirer les concepteurs et développeurs Web et aident également à améliorer leurs compétences.**

### On est là pour s'amuser avec du code. Prêt pour l'aventure ? Abonnes-toi maintenant...

### [**YouTube**](https://www.youtube.com/c/CodeOuvert) [**GitHub**](https://github.com/code-ouvert) [**GitLab**](https://gitlab.com/code-ouvert)

---
